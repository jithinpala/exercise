//
//  ListViewController.swift
//  Exercise
//
//  Created by Jithin Balan on 1/4/20.
//  Copyright © 2020 Jithin Balan. All rights reserved.
//

import UIKit

class ListViewController: UIViewController, UITableViewDataSource {

    private let tableview: UITableView = {
        let tableview = UITableView()
        return tableview
    }()
    private var viewModel: FactViewModel?
    private let factListManager = FactListManager()
    
    private let refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setUpTableView()
        requestFactItems()
    }
    
    private func setUpTableView() {
        view.addSubview(tableview)
        tableview.register(fromClass: FactItemTableViewCell.self)
        tableview.rowHeight = UITableView.automaticDimension
        tableview.dataSource = self
        tableview.refreshControl = refreshControl
        tableview.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                         leading: view.leadingAnchor,
                         trailing: view.trailingAnchor,
                         bottom: view.safeAreaLayoutGuide.bottomAnchor)
        tableview.separatorColor = UIColor.clear
        refreshControl.addTarget(self, action: #selector(pullToRefresh(_:)), for: .valueChanged)
    }
    
    private func requestFactItems() {
        guard ReachabilityManager.shared.isReachable else {
            showNoNetworkAlert()
            return
        }
        factListManager.retriveFactList { [weak self] result in
            switch result {
            case .success(let model):
                self?.updateListItems(for: model)
            case .failure(_):
                self?.updateListItems(for: nil)
                self?.showServerErrorAlert()
            }
        }
    }
    
    private func updateListItems(for viewModel: FactViewModel?) {
        self.viewModel = viewModel
        refreshControl.endRefreshing()
        tableview.reloadData()
        title = viewModel?.pageTitle
    }
    
    private func showNoNetworkAlert() {
        let alert = UIAlertController(title: FactListResourceHelper.Strings.noInternetConnectionTitle,
                                      message: nil,
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: FactListResourceHelper.Strings.noInternetConnectionAlertOkTitle,
                                      style: .default,
                                      handler: nil))
        present(alert, animated: true) {
            self.refreshControl.endRefreshing()
        }
    }
    
    private func showServerErrorAlert() {
        let alert = UIAlertController(title: FactListResourceHelper.Strings.serverConnectionError,
                                      message: nil,
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: FactListResourceHelper.Strings.noInternetConnectionAlertOkTitle,
                                    style: .default,
                                    handler: nil))
        present(alert, animated: true) {
            self.refreshControl.endRefreshing()
        }
    }
    
    @objc private func pullToRefresh(_ sender: UIRefreshControl) {
        requestFactItems()
    }
    
    // MARK: UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.rowCount ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: FactItemTableViewCell.identifier) as? FactItemTableViewCell else {
            return UITableViewCell()
        }
        let detailsViewModel = viewModel?.items[indexPath.row]
        cell.configure(for: detailsViewModel)
        return cell
    }
}
