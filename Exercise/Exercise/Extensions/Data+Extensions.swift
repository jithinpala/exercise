//
//  Data+Extensions.swift
//  Exercise
//
//  Created by Jithin Balan on 2/4/20.
//  Copyright © 2020 Jithin Balan. All rights reserved.
//

import Foundation

extension Data {
    func convertToUTF8() -> Data? {
        return String(data: self, encoding: .isoLatin1)?.data(using: .utf8)
    }
}
