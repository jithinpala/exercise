//
//  UITableView+Extensions.swift
//  Exercise
//
//  Created by Jithin Balan on 1/4/20.
//  Copyright © 2020 Jithin Balan. All rights reserved.
//

import UIKit

extension UITableView {
    /// Registers a cell from its class with a reuse identifier set to the class name.
    func register<T: UITableViewCell>(fromClass cellType: T.Type) {
        let identifier = String(describing: cellType)
        register(cellType, forCellReuseIdentifier: identifier)
    }
}
