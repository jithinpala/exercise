//
//  UIView+Extensions.swift
//  Exercise
//
//  Created by Jithin Balan on 1/4/20.
//  Copyright © 2020 Jithin Balan. All rights reserved.
//

import UIKit

extension UIView {
    func anchor(top: NSLayoutYAxisAnchor? = nil,
                leading: NSLayoutXAxisAnchor? = nil,
                trailing: NSLayoutXAxisAnchor? = nil,
                bottom: NSLayoutYAxisAnchor? = nil,
                padding: UIEdgeInsets = .zero,
                size: CGSize = .zero) {
        translatesAutoresizingMaskIntoConstraints = false
        
        if let top = top {
            topAnchor.constraint(equalTo: top, constant: padding.top).isActive = true
        }
        
        if let leading = leading {
            leadingAnchor.constraint(equalTo: leading, constant: padding.left).isActive = true
        }
        
        if let trailing = trailing {
            trailingAnchor.constraint(equalTo: trailing, constant: padding.right).isActive = true
        }
        
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: padding.bottom).isActive = true
        }
        
        if size.width != 0 {
            widthAnchor.constraint(equalToConstant: size.width).isActive = true
        }
        
        if size.height != 0 {
            heightAnchor.constraint(equalToConstant: size.height).isActive = true
        }
    }
    
    func anchorWithCenter(XAxisAnchor: NSLayoutXAxisAnchor? = nil,
                          YAxisAnchor: NSLayoutYAxisAnchor? = nil,
                          size: CGSize = .zero) {
        translatesAutoresizingMaskIntoConstraints = false
        
        if let XAxisAnchor = XAxisAnchor {
            centerXAnchor.constraint(equalTo: XAxisAnchor, constant: 0).isActive = true
        }
        
        if let YAxisAnchor = YAxisAnchor {
            centerYAnchor.constraint(equalTo: YAxisAnchor, constant: 0).isActive = true
        }
        
        if size.width != 0 {
            widthAnchor.constraint(equalToConstant: size.width).isActive = true
        }
        
        if size.height != 0 {
            heightAnchor.constraint(equalToConstant: size.height).isActive = true
        }
    }
}
