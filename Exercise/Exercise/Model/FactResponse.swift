//
//  Fact.swift
//  Exercise
//
//  Created by Jithin Balan on 1/4/20.
//  Copyright © 2020 Jithin Balan. All rights reserved.
//

import Foundation

struct FactResponse: Decodable {
    let title: String?
    let items: [DetailsResponse]?
    
    enum CodingKeys: String, CodingKey {
        case title
        case items = "rows"
    }
}

struct DetailsResponse: Decodable {
    let title: String?
    let description: String?
    let imagePath: String?
    
    enum CodingKeys: String, CodingKey {
        case title, description
        case imagePath = "imageHref"
    }
}
