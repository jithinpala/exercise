//
//  FactListManager.swift
//  Exercise
//
//  Created by Jithin Balan on 1/4/20.
//  Copyright © 2020 Jithin Balan. All rights reserved.
//

import Foundation

typealias FactListResult = (Result <FactViewModel, NSError>) -> Void

class FactListManager {
    
    private struct Constants {
        static let endpoint = "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json"
    }
    
    private struct Errors {
        static let responseError = NSError(domain: "FactListManager", code: 0,
                                           userInfo: [NSLocalizedDescriptionKey: "Invalid FactListManager JSON response"])
    }
    
    private var factListService: FactListService
    
    init(factListService: FactListService = FactListService(endPoint: Constants.endpoint)) {
        self.factListService = factListService
    }
    
    func retriveFactList(completion: @escaping FactListResult) {
        factListService.fetch { result in
            switch result {
            case .success(let data):
                let decoder = JSONDecoder()
                guard let dataNew = data.convertToUTF8(), let response = try? decoder.decode(FactResponse.self, from: dataNew)
                    else {
                    completion(.failure(Errors.responseError))
                    return
                }
                completion(.success(FactViewModel(response)))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
}
