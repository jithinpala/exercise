//
//  ReachabilityManager.swift
//  Exercise
//
//  Created by Jithin Balan on 3/4/20.
//  Copyright © 2020 Jithin Balan. All rights reserved.
//

import Foundation
import Reachability

class ReachabilityManager {
    
    static let shared = ReachabilityManager()
    let reachability = try? Reachability()
    
    var isReachable: Bool {
        guard let reachability = reachability else { return false }
        return reachability.connection != .unavailable
    }

    private init() { }
}
