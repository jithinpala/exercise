//
//  NetworkService.swift
//  Exercise
//
//  Created by Jithin Balan on 1/4/20.
//  Copyright © 2020 Jithin Balan. All rights reserved.
//

import Foundation
import UIKit

class NetworkService {
    
    public struct Keys {
        static let errorDomain = "DomainNetworking"
        static let errorResponseBody = "errorResponseBody"
        static let errorResponseJSON = "errorResponseJSON"
    }
    
    public private(set) var session: URLSession!
    
    /// Data task completion handler.
    public typealias DataTaskCompletionHandler = (Result<(URLResponse, Data), NSError>) -> Void
    
    /// Image task completion handler.
    public typealias ImageDownloadCompletionHandler = (Result<UIImage, NSError>) -> Void

    // MARK: Network Service

    public init(configuration: URLSessionConfiguration? = nil) {
        session = makeSession(configuration: configuration)
    }
    
    /// The default URLSession for handling network requests.
    private func makeSession(configuration: URLSessionConfiguration? = nil) -> URLSession {
        guard let configuration = configuration else {
            let config = URLSessionConfiguration.default
            return URLSession(configuration: config)
        }
        return URLSession(configuration: configuration)
    }

    func genericDataTask(with url: URL, completion: @escaping DataTaskCompletionHandler) {
        let task = session.dataTask(with: url) { (data, response, error) in
            DispatchQueue.main.async {
                if let error = error as NSError? {
                    completion(.failure(error))
                } else {
                    if let data = data, let response = response, let httpResponse = response as? HTTPURLResponse {
                        if (200...299).contains(httpResponse.statusCode) {
                            completion(.success((response, data)))
                        } else {
                            var userInfo: [String: Any] = [NSLocalizedDescriptionKey: HTTPURLResponse.localizedString(forStatusCode: httpResponse.statusCode), Keys.errorResponseBody: data]
                            if let errorJSON = (try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments)) as? [String: Any] {
                                userInfo[Keys.errorResponseJSON] = errorJSON
                            }
                            let serverError = NSError(domain: Keys.errorDomain, code: httpResponse.statusCode, userInfo: userInfo)
                            completion(.failure(serverError))
                        }
                    } else {
                        let unknown = NSError(domain: Keys.errorDomain, code: 0, userInfo: [NSLocalizedDescriptionKey: "Response did not contain any data"])
                        completion(.failure(unknown))
                    }
                    
                }
            }
        }
        task.resume()
    }
    
    // MARK: Downloading UIImage
    
    func getImage(withURL url: URL, completion: @escaping ImageDownloadCompletionHandler) {
        genericDataTask(with: url) { result in
            switch result {
            case .success(let (_, data)):
                guard let image = UIImage(data: data) else {
                    let imageCreationError = NSError(domain: Keys.errorDomain,
                                                     code: 2,
                                                     userInfo: [NSLocalizedDescriptionKey: "Could not create image from reponse data"])
                    completion(.failure(imageCreationError))
                    return
                }
                completion(.success(image))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
}
