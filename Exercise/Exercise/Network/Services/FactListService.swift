//
//  FactListService.swift
//  Exercise
//
//  Created by Jithin Balan on 1/4/20.
//  Copyright © 2020 Jithin Balan. All rights reserved.
//

import Foundation

typealias FactListServiceResult = (Result <Data, NSError>) -> Void

class FactListService {
    private let networkService = NetworkService()
    
    private let endPoint: String
    
    private enum Constants {
        static let errorDomain = "FactListService"
        static let invalidUrlLocalizedDescriptionKey = NSLocalizedString("Invalid URL", comment: "")
        static let unknownErrorLocalizedDescriptionKey = NSLocalizedString("Unknown error", comment: "")
    }
    
    private enum Errors {
        static let unknownError = NSError(domain: Constants.errorDomain, code: 0,
                                          userInfo: [NSLocalizedDescriptionKey: Constants.unknownErrorLocalizedDescriptionKey])
        static let invalidUrl = NSError(domain: Constants.errorDomain, code: 1,
                                        userInfo: [NSLocalizedDescriptionKey: Constants.invalidUrlLocalizedDescriptionKey])
    }
    
    init(endPoint: String) {
        self.endPoint = endPoint
    }
    
    /// Fetch facts list
    /// - Parameter completion: Completion block with response data
    func fetch(completion: @escaping FactListServiceResult) {
        guard let url = URL(string: endPoint)
            else {
                completion(.failure(Errors.invalidUrl))
                return
        }
        networkService.genericDataTask(with: url) { result in
            switch result {
            case .success(_, let data):
                guard !data.isEmpty else {
                    completion(.failure(Errors.unknownError))
                    return
                }
                completion(.success(data))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
