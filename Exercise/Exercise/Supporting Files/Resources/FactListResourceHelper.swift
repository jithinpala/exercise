//
//  FactListResourceHelper.swift
//  Exercise
//
//  Created by Jithin Balan on 3/4/20.
//  Copyright © 2020 Jithin Balan. All rights reserved.
//

import Foundation

struct FactListResourceHelper {
    
    enum Strings {
        static let tableName = "FactList"
        
        static func factLocalized(_ key: String) -> String {
            return localized(key, table: tableName)
        }
        
        static let noInternetConnectionTitle = factLocalized("no.internetConnection.error.title")
        static let noInternetConnectionAlertOkTitle = factLocalized("no.internetConnection.alertAction.okTitle")
        static let serverConnectionError = factLocalized("server.connection.error")
    }
}
