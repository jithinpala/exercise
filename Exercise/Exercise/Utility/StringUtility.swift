//
//  StringUtility.swift
//  Exercise
//
//  Created by Jithin Balan on 3/4/20.
//  Copyright © 2020 Jithin Balan. All rights reserved.
//

import Foundation

func localized(_ key: String, table: String?) -> String {
    if table != nil {
        let localized = NSLocalizedString(key, tableName: table, bundle: Bundle.main, comment: "")
        if localized != key {
            return localized
        }
    }
    return NSLocalizedString(key, comment: "")
}
