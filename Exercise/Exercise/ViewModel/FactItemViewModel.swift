//
//  FactItemViewModel.swift
//  Exercise
//
//  Created by Jithin Balan on 2/4/20.
//  Copyright © 2020 Jithin Balan. All rights reserved.
//

import Foundation

struct FactItemViewModel {
    var itemTitle: String?
    var description: String?
    var imageURL: URL?
    
    init(_ itemResponse: DetailsResponse) {
        itemTitle = itemResponse.title
        description = itemResponse.description
        if let imagePath = itemResponse.imagePath {
            imageURL = URL(string: imagePath)
        }
    }
}
