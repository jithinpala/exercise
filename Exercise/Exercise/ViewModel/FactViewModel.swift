//
//  FactViewModel.swift
//  Exercise
//
//  Created by Jithin Balan on 2/4/20.
//  Copyright © 2020 Jithin Balan. All rights reserved.
//

import Foundation

struct FactViewModel {
    let pageTitle: String?
    var items = [FactItemViewModel]()
    
    init(_ response: FactResponse) {
        pageTitle = response.title
        response.items?.forEach {
            if $0.title != nil {
                items.append(FactItemViewModel($0))
            }
        }
    }
    
    var rowCount: Int {
        return items.count
    }
    
}
