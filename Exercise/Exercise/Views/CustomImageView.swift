//
//  CustomImageView.swift
//  Exercise
//
//  Created by Jithin Balan on 2/4/20.
//  Copyright © 2020 Jithin Balan. All rights reserved.
//

import UIKit

let imageCache = NSCache<AnyObject, AnyObject>()

class CustomImageView: UIImageView {
    var imageUrlString: String?
    
    func loadImageURL(_ url: URL?, completion: @escaping ((_ image: UIImage?) -> Void)) {
        image = nil
        guard let url = url else { return }
        imageUrlString = url.absoluteString
        if let imageFromCache = imageCache.object(forKey: url.absoluteString as AnyObject) {
            guard imageUrlString == url.absoluteString else {
                completion(nil)
                return
            }
            let cachedImage = imageFromCache as? UIImage
            completion(cachedImage)
            return
        }
        NetworkService().getImage(withURL: url) { result in
            switch result {
            case .success(let image):
                imageCache.setObject(image, forKey: url.absoluteString as AnyObject)
                if self.imageUrlString == url.absoluteString {
                    completion(image)
                }
            case .failure(_):
                completion(nil)
            }
        }
    }
}
