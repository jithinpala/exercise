//
//  FactItemTableViewCell.swift
//  Exercise
//
//  Created by Jithin Balan on 1/4/20.
//  Copyright © 2020 Jithin Balan. All rights reserved.
//

import UIKit

class FactItemTableViewCell: UITableViewCell {

    static let identifier: String = "FactItemTableViewCell"
    
    private let title: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.backgroundColor = .clear
        label.font = UIFont.systemFont(ofSize: 18.0, weight: .bold)
        return label
    }()
    
    private let thumbImage: CustomImageView = {
        let imageView = CustomImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private let textView: UITextView = {
        let textView = UITextView()
        textView.isEditable = false
        textView.isScrollEnabled = false
        textView.font = UIFont.systemFont(ofSize: 16.0, weight: .light)
        return textView
    }()
    
    private let bottomDivider: UIView = {
        let view = UIView()
        view.backgroundColor = .gray
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    private func setupViews() {
        addSubview(title)
        addSubview(textView)
        addSubview(bottomDivider)
        
        title.anchor(top: topAnchor,
                     leading: leadingAnchor,
                     trailing: trailingAnchor,
                     padding: .init(top: 10, left: 10, bottom: 0, right: -10))
        title.bottomAnchor.constraint(equalTo: textView.topAnchor, constant: -10).isActive = true
        textView.anchor(leading: leadingAnchor,
                        trailing: trailingAnchor,
                        padding: .init(top: 0, left: 10, bottom: -20, right: 0))
        textView.bottomAnchor.constraint(equalTo: bottomDivider.topAnchor, constant: -10).isActive = true
        bottomDivider.anchor(leading: leadingAnchor,
                             trailing: trailingAnchor,
                             bottom: bottomAnchor,
                             size: CGSize(width: 0, height: 1))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(for viewModel: FactItemViewModel?) {
        title.text = viewModel?.itemTitle
        textView.text = viewModel?.description
        textView.textContainer.exclusionPaths = []
        thumbImage.loadImageURL(viewModel?.imageURL) { [weak self] image in
            guard let image = image else { return }
            let resizedImage = image.resizeImage(image: image, targetSize: CGSize(width: 80, height: 80))
            self?.addThumbImage(image: resizedImage)
            let imagePath = UIBezierPath(rect: CGRect(x: 0, y: 0, width: resizedImage.size.width + 5, height: resizedImage.size.height))
            self?.textView.textContainer.exclusionPaths = [imagePath]
        }
    }
    
    private func addThumbImage(image: UIImage) {
        addSubview(thumbImage)
        thumbImage.anchor(leading: leadingAnchor,
                          padding: .init(top: 0, left: 10, bottom: 0, right: 0))
        thumbImage.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 10).isActive = true
        thumbImage.bottomAnchor.constraint(lessThanOrEqualTo: bottomDivider.topAnchor, constant: -20).isActive = true
        thumbImage.image = image
    }

}
