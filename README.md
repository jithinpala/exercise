# README #

This is an iOS Proficiency Exercise. This creates an App which ingests a [json feed](https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json) and displays the data in a table.

* Technologies used : Swift version 5.0
* Version : iOS 11
* Supporting devices: Support both iPhone and iPad
* Frameworks Used(cocoapods): pod 'ReachabilitySwift'

